--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2017-08-03
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-trans-invoices/sql/postgresql/upgrade/upgrade-4.1.0.0.3-4.1.0.0.4.sql','');


-- Table to store the history of prices paid in projects.
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_trans_price_history';
	IF 0 != v_count THEN return 0; END IF;
	create table im_trans_price_history (
		project_id		integer
					constraint im_trans_price_history_project_fk
					references im_projects,
		uom_id			integer not null
					constraint im_trans_price_history_uom_fk
					references im_categories,
		company_id		integer not null
					constraint im_trans_price_history_company_fk
					references im_companies,
		task_type_id		integer
					constraint im_trans_price_history_task_type_fk
					references im_categories,
		target_language_id	integer
					constraint im_trans_price_history_target_fk
					references im_categories,
		source_language_id	integer
					constraint im_trans_price_history_source_flg
					references im_categories,
		subject_area_id		integer
					constraint im_trans_price_history_subject_fk
					references im_categories,
		file_type_id		integer
					constraint im_trans_price_history_file_type_fk
					references im_categories,
		valid_from		timestamptz,
		valid_through		timestamptz,
					-- make sure the end date is after start date
					constraint im_trans_price_history_date_const
					check(valid_through - valid_from >= '0 seconds'::interval),
		--
		-- "Output variables"
		currency		char(3) references currency_codes(ISO)
					constraint im_trans_price_history_currency_nn
					not null,
		price			numeric(12,4)
					constraint im_trans_price_history_price_nn
					not null,
		min_price		numeric(12,4),
		note			text,
		creation_date		timestamptz
	);

	-- make sure the same price doesn't get defined twice
	create unique index im_trans_price_history_idx on im_trans_price_history (
		project_id,uom_id, company_id, task_type_id, target_language_id,
		source_language_id, subject_area_id, file_type_id, currency
	);
	
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0() RETURNS integer AS $$
DECLARE
   v_price record;
BEGIN
		delete from im_trans_price_history;
		for v_price in select uom_id, company_id, task_type_id, target_language_id, source_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note from im_trans_prices
		loop
	insert into im_trans_price_history (uom_id, company_id, task_type_id, target_language_id, source_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note, creation_date) values (v_price.uom_id, v_price.company_id, v_price.task_type_id, v_price.target_language_id, v_price.source_language_id, v_price.subject_area_id, v_price.file_type_id, v_price.valid_from, v_price.valid_through, v_price.currency, v_price.price, v_price.min_price, v_price.note, now());
		end loop;

		return 0;
END;
$$ LANGUAGE plpgsql;
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();