# /packages/intranet-trans-invoices/tcl/intranet-trans-invoices-procs.tcl

ad_library {
    Bring together all "components" (=HTML + SQL code)
    related to Invoices

    @author frank.bergmann@project-open.com
    @creation-date  27 June 2003
}


# ------------------------------------------------------
# Price List
# ------------------------------------------------------

ad_proc im_trans_price_component { user_id company_id return_url} {
    Returns a formatted HTML table representing the
    prices for the current company
} {
    if {![im_permission $user_id view_costs]} { return "" }
    
    set enable_file_type_p [parameter::get_from_package_key -package_key intranet-trans-invoices -parameter "EnableFileTypeInTranslationPriceList" -default 0]

    set bgcolor(0) " class=roweven "
    set bgcolor(1) " class=rowodd "
    set price_format "000.000"
    set min_price_format "000.00"
    set price_url_base "/intranet-trans-invoices/price-lists/new"

    set colspan 10
    if {$enable_file_type_p} { incr colspan}

    set file_type_html "<td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.File_Type "File Type"]</td>"
    if {!$enable_file_type_p} { set file_type_html "" }

    set price_list_html "
    	<form action=/intranet-trans-invoices/price-lists/price-action method=POST>
	[export_vars -form {company_id return_url}]
	<table border=0>
	<tr><td colspan=$colspan class=rowtitle align=center>[_ intranet-trans-invoices.Price_List]</td></tr>
	<tr class=rowtitle> 
		  <td class=rowtitle>[_ intranet-trans-invoices.UoM]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Task_Type]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Source]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Target]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Subject]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Complexity_Type]</td>
		  $file_type_html
		  <td class=rowtitle>[_ intranet-trans-invoices.Rate]</td>
		  <td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.Minimum_Rate "Min Rate"]</td>
		  <td class=rowtitle>[_ intranet-core.Note]</td>
		  <td class=rowtitle>[im_gif -translate_p 1 del "Delete"]</td>
	</tr>
    "

    set price_rows_html ""
    set ctr 1
    set old_currency ""
	set price_status_id [cog::price::status::active]
    db_foreach prices {

select
	p.*,
	c.company_path as company_short_name,
	im_category_from_id(material_uom_id) as uom,
	im_category_from_id(task_type_id) as task_type,
	im_category_from_id(target_language_id) as target_language,
	im_category_from_id(source_language_id) as source_language,
	im_category_from_id(subject_area_id) as subject_area,
	im_category_from_id(file_type_id) as file_type,
	im_category_from_id(complexity_type_id) as complexity_type,
	to_char(min_price, :min_price_format) as min_price_formatted
from
	im_materials m,
	im_prices p
      LEFT JOIN
	im_companies c USING (company_id)
where
	p.company_id=:company_id
	and p.material_id = m.material_id
	and p.price_status_id = :price_status_id
order by
	im_category_from_id(task_type_id) desc,
	im_category_from_id(subject_area_id) desc,
	im_category_from_id(source_language_id) desc,
	im_category_from_id(target_language_id) desc,
	currency,
	material_uom_id
} {

        # There can be errors when formatting an empty string...
        set price_formatted ""
        catch { set price_formatted "[format "%0.3f" $price] $currency" } errmsg
	if {"" != $old_currency && ![string equal $old_currency $currency]} {
	    append price_rows_html "<tr><td colspan=$colspan>&nbsp;</td></tr>\n"
	}

        set price_url [export_vars -base $price_url_base { company_id price_id return_url }]

	set file_type_html "<td>$file_type</td>"
	if {!$enable_file_type_p} { set file_type_html "" }

	append price_rows_html "
        <tr $bgcolor([expr $ctr % 2]) nobreak>
	  <td>$uom</td>
	  <td>$task_type</td>
	  <td>$source_language</td>
          <td>$target_language</td>
	  <td>$subject_area</td>
	  <td>$complexity_type</td>
	  $file_type_html
          <td><a href=\"$price_url\">$price_formatted</a></td>
          <td>$min_price_formatted</td>
          <td>[string_truncate -len 15 $note]</td>
          <td><input type=checkbox name=price_id.$price_id></td>
	</tr>"
	incr ctr
	set old_currency $currency
    }

    if {$price_rows_html != ""} {
	append price_list_html $price_rows_html
    } else {
	append price_list_html "<tr><td colspan=$colspan align=center><i>[_ intranet-trans-invoices.No_prices_found]</i></td></tr>\n"
    }

    set sample_pracelist_link "<a href=/intranet-trans-invoices/price-lists/pricelist_sample.csv>[_ intranet-trans-invoices.lt_sample_pricelist_CSV_]</A>"

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<tr>
	  <td colspan=$colspan align=right>
	    <input type=submit name=add_new value=\"[_ intranet-trans-invoices.Add_New]\">
	    <input type=submit name=del value=\"[_ intranet-trans-invoices.Del]\">
	  </td>
	</tr>
        "
    }

    append price_list_html "
       </table>
       </form>
    "

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<ul>
	  <li>
	    <a href=/intranet-trans-invoices/price-lists/upload-prices?[export_vars -url {company_id return_url}]>
	      [_ intranet-trans-invoices.Upload_prices]</A>
	    [_ intranet-trans-invoices.lt_for_this_company_via_]
	  <li>
	    <a href=\"[export_vars -base "/intranet-reporting/view" {{report_code price_list_export} {format csv} company_id}]\">
	    [lang::message::lookup "" intranet-trans-invoices.Export_as_csv "Export price list as CSV"]
	    </a>
	</ul>
        "
    }

    return $price_list_html
}


# ---------------------------------------------------------------
# Permissions
# ---------------------------------------------------------------

ad_proc -public im_trans_invoice_permissions {
    {-debug 0}
    current_user_id
    invoice_id
    view_var
    read_var
    write_var
    admin_var
} {
    Fill the "by-reference" variables read, write and admin
    with the permissions of $current_user_id on $invoice_id
} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

	set perm_proc [im_parameter -package_id [im_package_invoices_id] "InvoicePermissionProc"]
	
	$perm_proc $current_user_id $invoice_id view read write admin

    if {$debug} { ns_log Notice "im_trans_invoice_permissions: cur=$current_user_id, user=$user_id, view=$view, read=$read, write=$write, admin=$admin" }

}

ad_proc -public im_trans_invoice_create_from_tasks {
    {-cost_type_id ""}
    {-invoice_id ""}
    {-task_ids ""}
    {-cost_center_id ""}
	{-current_user_id ""}
    {-project_id ""}
} {
    Create a quote / invoice from the tasks. This is CUSTOMER Facing and uses Customer Pricing.
    
    @param project_id Project ID for which to create the translation invoice
    @param invoice_id Invoice ID we want to UPDATE. In this case the line items will be removed and new line items generated for the tasks in the project
    @param task_ids List of TaskIDs which will be used to limit the invoice to only include these tasks. Helpful if you want to create a partial invoice
    @param cost_center_id Cost center to use
    @param cost_type_id Cost Type for the invoice. Defaults to quote, but could also be an actual customer invoice.
} {
    if {$cost_type_id eq ""} {set cost_type_id [im_cost_type_quote]}
    
	if {$project_id eq ""} {
		set first_task_id [lindex $task_ids 0]
        set project_id [db_string project_id_from_task_id "select project_id from im_trans_tasks where task_id =:first_task_id limit 1" -default ""]
	} 

	if {$project_id eq ""} {
		return
		ad_script_abort
	}

    # Get info about the project
    db_1row project_info "select company_id,project_lead_id,subject_area_id,project_cost_center_id,complexity_type_id from im_projects where project_id = :project_id"
    
    if {$cost_center_id eq ""} {
		set cost_center_id $project_cost_center_id
    }
    
	if {$task_ids eq ""} {
    	set num_tasks [db_string tasks "select count(task_id) from im_trans_tasks where project_id = :project_id and billable_units > 0" -default 0]
	} else {
    	set num_tasks [db_string tasks "select count(task_id) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) and billable_units > 0" -default 0]
	}
    
    if {$num_tasks eq 0} {
		return
		ad_script_abort
    }
    
    # ---------------------------------------------------------------
    # Create the Quote
    # ---------------------------------------------------------------
    
    # Get the payment days from the company
    set payment_term_id [db_string default_payment_days "select payment_term_id from im_companies where company_id = :company_id" -default ""]
    set payment_days ""
    if {"" != $payment_term_id} {
		set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
    }
    if {$payment_days == ""} {
		set payment_days [im_parameter -package_id [im_package_cost_id] "DefaultCompanyInvoicePaymentDays" "" 30]
    }
    
    set provider_id [im_company_internal]
	
    # get company information
    db_1row company_info "select
		c.*,
		o.*,
		cc.country_name
	from
		im_companies c
	  	LEFT JOIN im_offices o ON c.main_office_id=o.office_id
	  	LEFT JOIN country_codes cc ON o.address_country_code=cc.iso
	where
		c.company_id = :company_id"
	
    set company_contact_id [im_invoices_default_company_contact $company_id $project_id]

    # First we check for project company_office_id
    set invoice_office_id [db_string company_accounting_office "select company_office_id from im_projects where project_id = :project_id" -default ""]

    # If that 'company_office_id' in 'im_projects' is null, we use main_office_id as value
    if {$invoice_office_id eq ""}  {
        set invoice_office_id [db_string company_main_office_info "select main_office_id from im_companies where company_id = :company_id" -default ""]
    }
    # Get some defaults if missing from the internal company
    if {$default_quote_template_id eq ""} {
	set default_quote_template_id [db_string template "select default_quote_template_id from im_companies where company_id = [im_company_internal]" -default ""]
    }
	
    set invoice_date [db_string get_today "select now()::date"]
    
    if {$invoice_id eq ""} {
		# Create the invoice
		set cost_status_id [im_cost_status_created]
		set invoice_id [im_new_object_id]
		set invoice_nr [im_next_invoice_nr -cost_type_id $cost_type_id -cost_center_id $cost_center_id]
		
		# ---------------------------------------------------------------
		# GET THE CORRECT VAT (MISSING)
		# ---------------------------------------------------------------
		
		db_exec_plsql create_invoice {
			select im_trans_invoice__new (
						:invoice_id,
						'im_trans_invoice',
						now(),
						:project_lead_id,
						'0.0.0.0',
						null,
						:invoice_nr,
						:company_id,
						:provider_id,
						:company_contact_id,
						:invoice_date,
						'EUR',
						:default_quote_template_id,
						:cost_status_id,
						:cost_type_id,
						:default_payment_method_id,
						:payment_days,
						'0',
						:default_vat,
						:default_tax,
						null
				);
		}   
	
		# Not sure about this one, as invoice_office_id was here before, but was never used during creation of invoice
		if {$invoice_id ne ""} {
	       db_dml update_invoice_office_id "update im_invoices set invoice_office_id = :invoice_office_id where invoice_id =:invoice_id"
        }

    } else {
		db_1row invoice_info "select invoice_nr, cost_status_id from im_invoices,im_costs where invoice_id = cost_id and invoice_id = :invoice_id"
		
		# invoice exists, remove all line items
		db_dml delete_invoice_items "
				DELETE from im_invoice_items
				WHERE invoice_id=:invoice_id
			"
		# Update the effective date
		db_dml update_date "update im_costs set effective_date = :invoice_date where cost_id = :invoice_id"
    }
    
    # Update cost_center
    db_dml update_cost_center "update im_costs set cost_center_id = :cost_center_id where cost_id = :invoice_id"
    
    # ---------------------------------------------------------------
    # Now add the tasks as line items to the invoice
    # ---------------------------------------------------------------
    set sort_order 1
    set currency [im_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
    

	#---------------------------------------------------------------
	# First take a look at minimum price
	#---------------------------------------------------------------
    
	# Check if we have languages with the minimum rate.
    set min_price_languages [list]

	set min_price_task_sql "
		select sum(billable_units) as sum_tasks, array_to_string(array_agg(task_name), ',') as task_name_agg, 
		target_language_id, task_uom_id, task_type_id, source_language_id, im_file_type_from_trans_task(task_id) as file_type_id 
		from im_trans_tasks 
		where project_id = :project_id
		and billable_units >0"

	if {$task_ids ne ""} {
		append min_price_task_sql " and task_id in ([template::util::tcl_to_sql_list $task_ids])"
    }
	
	# Append the grouping
	append min_price_task_sql "\n group by target_language_id, task_uom_id, task_type_id, source_language_id, file_type_id"

    db_foreach tasks $min_price_task_sql {
		set rate_info [im_trans_invoice_rate \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $subject_area_id \
			-target_language_id $target_language_id \
			-source_language_id $source_language_id \
			-complexity_type_id $complexity_type_id \
			-currency $currency \
			-task_uom_id $task_uom_id \
			-task_sum $sum_tasks
			]
	
		# Split the rate information up into the components
		set billable_units [lindex $rate_info 0]
		set rate_uom_id [lindex $rate_info 1]
		set rate [lindex $rate_info 2]
		
		# We found a language where the tasks together are below the
		# Minimum Price.
		if {[im_uom_unit] eq $rate_uom_id && [im_uom_unit] ne $task_uom_id} {
			lappend min_price_languages $target_language_id
			
			set material_id [im_trans_material -target_language_id $target_language_id -source_language_id $source_language_id -file_type_id $file_type_id -complexity_type_id $complexity_type_id -task_type_id $task_type_id -task_uom_id $task_uom_id]
			if {$material_id eq ""} {				
				set material_id [im_trans_material_create_from_parameters -source_language_id $source_language_id -target_language_id $target_language_id -task_type_id $task_type_id -task_uom_id $task_uom_id] 
			}
			
			# Insert the min price
			
			set task_list [split $task_name_agg ","]
			if {[llength $task_list] >1} {
				set task_name " [join $task_list "<br>"]"
			} else {
				set task_name [lindex $task_list 0]
			}

			set newly_created_invoice_item_id [db_string new_invoice_item "SELECT im_invoice_item__new(
				null, 'im_invoice_item', now(), :current_user_id, '[ad_conn peeraddr]', null,
				:task_name, :invoice_id, :sort_order,
				:billable_units, :rate_uom_id, :rate, :currency,
				:task_type_id, [im_invoice_item_status_active]
			)"]

			if {$newly_created_invoice_item_id ne ""} {
				db_dml update_material_id "update im_invoice_items set project_id = :project_id, item_material_id = :material_id where item_id = :newly_created_invoice_item_id"
			}

			incr sort_order
		}
    }
    
	#---------------------------------------------------------------
	# Now work with normal prices
	#---------------------------------------------------------------

    set task_sql "select task_type_id,
		target_language_id,
		source_language_id,
		task_uom_id,
		task_name,
		billable_units,
		task_id,
		im_file_type_from_trans_task(task_id) as file_type_id
	from im_trans_tasks where project_id = :project_id and billable_units >0"
    
    if {$task_ids ne ""} {
		append task_sql " and task_id in ([template::util::tcl_to_sql_list $task_ids])"
    }
    
    # Order by filename and language
    append task_sql " order by task_name, im_name_from_id(target_language_id)"

    db_foreach tasks $task_sql {
	
		if {[lsearch $min_price_languages $target_language_id]<0 } {
			set material_id [im_trans_material -target_language_id $target_language_id -source_language_id $source_language_id -file_type_id $file_type_id -complexity_type_id $complexity_type_id -task_type_id $task_type_id -task_uom_id $task_uom_id]
			if {$material_id eq ""} {
				set material_id [im_trans_material_create_from_parameters -source_language_id $source_language_id -target_language_id $target_language_id -task_type_id $task_type_id -task_uom_id $task_uom_id] 
			}
			
			# Ignore the minimum rate
			set rate_info [im_trans_invoice_rate \
					-company_id $company_id \
					-task_type_id $task_type_id \
					-subject_area_id $subject_area_id \
					-target_language_id $target_language_id \
					-source_language_id $source_language_id \
					-currency $currency \
					-task_uom_id $task_uom_id \
					-file_type_id $file_type_id \
					-complexity_type_id $complexity_type_id \
					-task_sum $billable_units \
					-ignore_min_price
				]
				
			# Split the rate information up into the components
			set billable_units [lindex $rate_info 0]
			set task_uom_id [lindex $rate_info 1]
			set rate [lindex $rate_info 2]
						
			set newly_created_invoice_item_id [db_string new_invoice_item "SELECT im_invoice_item__new(
				null, 'im_invoice_item', now(), :current_user_id, '[ad_conn peeraddr]', null,
				:task_name, :invoice_id, :sort_order,
				:billable_units, :task_uom_id, :rate, :currency,
				:task_type_id, [im_invoice_item_status_active]
			)"]

			if {$newly_created_invoice_item_id ne ""} {
				db_dml update_material_id "update im_invoice_items set project_id = :project_id, item_material_id = :material_id, task_id = :task_id where item_id = :newly_created_invoice_item_id"
			}
			incr sort_order
		}
    }
    
    # Update the invoice amount based on the invoice items
    im_invoice_update_rounded_amount -invoice_id $invoice_id
    
	#---------------------------------------------------------------
	# Insert project relations
	#---------------------------------------------------------------
    db_dml update_cost "update im_costs set project_id = :project_id where cost_id = :invoice_id"
    
    db_1row "get relations" "
        			select  count(*) as v_rel_exists
        			from    acs_rels
        			where   object_id_one = :project_id
        					and object_id_two = :invoice_id
        "
    if {0 ==  $v_rel_exists} {
    	set rel_id [db_exec_plsql create_invoice_rel "	  select acs_rel__new (
      	   null,             -- rel_id
      	   'relationship',   -- rel_type
      	   :project_id,      -- object_id_one
      	   :invoice_id,      -- object_id_two
      	   null,             -- context_id
      	   null,             -- creation_user
      	   null             -- creation_ip
      )"]
    }
    	
    db_release_unused_handles
    
    # Audit creation
    cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id $cost_status_id -type_id $cost_type_id
    
    return $invoice_id
}

ad_proc -public im_trans_material_create_from_parameters {
	-source_language_id:required
	-target_language_id:required
	-task_type_id:required
	-task_uom_id:required
} {
	Generate a material for translation

	@param source_language_id Source language
	@param target_language_id Target language
	@param task_type_id Task Type (from "Intranet Project Type")
	@param task_uom_id Unit of Measure for this

	@return material_id generated material_id
} {
	set material_type_id [im_material_type_auto_gen]
	set material_name "[im_name_from_id $source_language_id], [im_name_from_id $target_language_id], [im_name_from_id $task_type_id]"
	regsub -all { } [string trim [string tolower $material_name]] "" material_nr
	
	set material_id [db_string existing_material "select material_id from im_materials where material_nr = :material_nr" -default ""]
	if {$material_id eq ""} {
		set material_id [db_string material_create " \
			select im_material__new ( \
				[db_nextval acs_object_id_seq], 'im_material', now(), [ad_conn user_id], '[ns_conn peeraddr]', null, \
				:material_name, :material_nr, :material_type_id, [im_material_status_active], \
				:task_uom_id, 'Automatically generated' \
			)"]

		db_dml update_material "update im_materials 
			set source_language_id = :source_language_id,
				target_language_id = :target_language_id,
				task_type_id = :task_type_id
			where material_id = :material_id"
	}

	return $material_id

}

ad_proc -public im_trans_material {
	{-task_type_id ""}
	{-file_type_id ""}
	{-complexity_type_id ""}
	{-source_language_id ""}
	{-target_language_id ""}
	{-task_uom_id ""}
} {
	Returns the material_id for the parameters provided.

} {

	set orig_source_language_id $source_language_id
	set orig_target_language_id $target_language_id
	set orig_task_type_id $task_type_id

	# Initial SQL command
	set sql "select min(material_id) from im_materials where 1=1"
	
	# Check if the param is set
	set params [list task_type_id file_type_id source_language_id target_language_id]
	foreach param $params {
		if {[set $param] ne ""} {
			append sql "\t and $param = [set $param]\n"
		} else {
			append sql "\t and $param is null\n"
		}
	}
	append sql "\t LIMIT 1 \n"
	
	set material_id [db_string existing_material $sql -default ""]
	if {$material_id eq ""} {
		
		# If the material can't be found, try to find it based on the best price match
		# Note that for the material we can utterly ignore the subject area id
		set best_match_price_sql "select
			im_trans_prices_calc_relevancy (
				p.company_id,[im_company_internal],
				p.task_type_id, :task_type_id,
				p.subject_area_id, null,
				p.target_language_id, :target_language_id,
				p.source_language_id, :source_language_id,
				p.file_type_id, :file_type_id,
				p.complexity_type_id, :complexity_type_id
			) as relevancy,
			p.task_type_id,
			p.target_language_id,
			p.source_language_id,
			p.file_type_id
		from im_trans_prices p
		where
			uom_id=:task_uom_id
		order by relevancy desc
		limit 1"
		
		if {[db_0or1row	price_match $best_match_price_sql]} {
			# We overwrite the variables, so we can now run the material finding again
			set material_id [db_string existing_material $sql -default ""]
		}
	}

	if {$material_id eq "" && $task_uom_id ne ""} {
		set material_id [im_trans_material_create_from_parameters \
			-source_language_id $orig_source_language_id \
			-target_language_id $orig_target_language_id \
			-task_type_id $orig_task_type_id \
			-task_uom_id $task_uom_id]
	}
	return $material_id
}