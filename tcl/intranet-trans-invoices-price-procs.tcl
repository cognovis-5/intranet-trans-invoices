# /packages/intranet-trans-invoices/tcl/intranet-trans-invoices-price-procs.tcl

ad_library {

	Price procs
	
	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_translation_best_match_price {
	-company_id
	-task_type_id
	{-subject_area_id ""}
	-target_language_id
	-source_language_id
	{-file_type_id ""}
	{-complexity_type_id ""}
	{-invoice_currency "EUR"}
	-task_uom_id
	-task_sum
	{-ignore_min_price_p "0"}
} {
	Calculate the best match price for a customer.
	Complicated undertaking, because the price depends on a number of variables, depending on client etc. As a solution, we act like a search engine, return all prices and rank them according to relevancy. We take only the first (=highest rank) line for the actual price proposal.


} {
	set number_format "9999990.099"
	set references_prices_sql "
		select
			p.price_id as best_match_price_id,
			p.relevancy as price_relevancy,
			p.price,
			trim(' ' from to_char(p.price,:number_format)) as best_match_price,
			p.min_price as best_match_min_price,
			trim(' ' from to_char(p.min_price,:number_format)) as min_price_formatted,
			p.company_id as price_company_id,
			p.uom_id as uom_id,
			p.task_type_id as task_type_id,
			p.target_language_id as target_language_id,
			p.source_language_id as source_language_id,
			p.subject_area_id as subject_area_id,
			p.file_type_id as file_type_id,
			p.complexity_type_id as complexity_type_id,
			p.valid_from,
			p.valid_through,
			p.price_note,
			c.company_path as price_company_name,
				im_category_from_id(p.uom_id) as price_uom,
				im_category_from_id(p.task_type_id) as price_task_type,
				im_category_from_id(p.target_language_id) as price_target_language,
				im_category_from_id(p.source_language_id) as price_source_language,
				im_category_from_id(p.subject_area_id) as price_subject_area,
				im_category_from_id(p.file_type_id) as price_file_type,
				im_category_from_id(p.complexity_type_id) as price_complexity_type
		from
			(
				(select
					im_trans_prices_calc_relevancy (
						p.company_id,:company_id,
						p.task_type_id, :task_type_id,
						p.subject_area_id, :subject_area_id,
						p.target_language_id, :target_language_id,
						p.source_language_id, :source_language_id,
						p.file_type_id, :file_type_id,
						p.complexity_type_id, :complexity_type_id
					) as relevancy,
					p.price_id,
					p.price,
					p.min_price,
					p.company_id,
					p.uom_id,
					p.task_type_id,
					p.target_language_id,
					p.source_language_id,
					p.subject_area_id,
					p.file_type_id,
					p.complexity_type_id,
					p.valid_from,
					p.valid_through,
					p.note as price_note
				from im_trans_prices p
				where
					uom_id=:task_uom_id
					and currency=:invoice_currency
				)
			) p,
			im_companies c
		where
			p.company_id=c.company_id
			and relevancy >= 0
		order by
			p.relevancy desc,
			p.company_id,
			p.uom_id
		limit 1
	"

	if {[db_0or1row reference_prices $references_prices_sql]} {

		ns_log Debug "::: $price_company_name :: $price_relevancy :: $price_target_language :: $price_source_language :: $price_subject_area"

		regsub -all {,} $best_match_price {.} best_match_price
		regsub -all {,} $task_sum {.} task_sum

		if {!$ignore_min_price_p} {
			if {[expr $best_match_price * $task_sum] < $best_match_min_price} {
				set task_sum 1
				set task_uom_id [im_uom_unit]
				set task_uom [im_category_from_id -translate_p 0 $task_uom_id]
				set best_match_price $best_match_min_price
			}
		}
	} else {
		set best_match_price 0
		set best_match_min_price 0
	}
	return [list $task_sum $task_uom_id $best_match_price]
}


ad_proc -public im_trans_invoice_rate {
	-company_id
	-task_type_id
	{-subject_area_id ""}
	-target_language_id
	-source_language_id
	{-file_type_id ""}
	{-complexity_type_id ""}
	{-currency "EUR"}
	-task_uom_id
	-task_sum
	-ignore_min_price:boolean
} {
	Calculate the best rate for a customer.

	Supports looping up through the prices for parent categories
} {
	set rate_info [im_translation_best_match_price \
		-company_id $company_id \
		-task_type_id $task_type_id \
		-subject_area_id $subject_area_id \
		-target_language_id $target_language_id \
		-source_language_id $source_language_id \
		-invoice_currency $currency \
		-task_uom_id $task_uom_id \
		-file_type_id $file_type_id \
		-complexity_type_id $complexity_type_id \
		-task_sum $task_sum \
		-ignore_min_price_p $ignore_min_price_p
	]

	set rate [lindex $rate_info 2]
	if {$rate eq 0 || $rate eq ""} {
		# No rate found, try with the parent languages
	   	set parent_source_language_id [lindex [im_category_parents $source_language_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $subject_area_id \
			-target_language_id $target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-complexity_type_id $complexity_type_id \
			-task_sum $task_sum \
	 		-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with the parent_source and subject_area
		set parent_subject_area_id [lindex [im_category_parents $subject_area_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $parent_subject_area_id \
			-target_language_id $target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-complexity_type_id $complexity_type_id \
			-task_sum $task_sum \
		 	-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with the parent_target
		set parent_target_language_id [lindex [im_category_parents $target_language_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $subject_area_id \
			-target_language_id $parent_target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-complexity_type_id $complexity_type_id \
			-task_sum $task_sum \
		 	-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with all the parents
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $parent_subject_area_id \
			-target_language_id $parent_target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-complexity_type_id $complexity_type_id \
			-task_sum $task_sum \
			-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}
	return $rate_info
}

ad_proc -public im_translation_best_rate {
	-provider_id
	-task_type_id
	{-subject_area_id ""}
	-target_language_id
	-source_language_id
	{-currency "EUR"}
	-task_uom_id
	{-task_sum "0"}
	{-ignore_min_price_p "0"}
} {
	return [util_memoize [list im_translation_best_rate_helper \
		-provider_id $provider_id \
		-task_type_id $task_type_id \
		-subject_area_id $subject_area_id \
		-target_language_id $target_language_id \
		-source_language_id $source_language_id \
		-currency $currency \
		-task_sum $task_sum \
		-ignore_min_price_p $ignore_min_price_p \
		-task_uom_id $task_uom_id] 3600]
}

ad_proc -public im_translation_best_rate_helper {
	-provider_id
	-task_type_id
	-subject_area_id
	-target_language_id
	-source_language_id
	-currency
	-task_uom_id
	-task_sum
	-ignore_min_price_p

} {
	Calculate the best rate for freelancers
} {
	set number_format "9999990.099"

 set references_prices_sql "
	 select
		 p.price_id as best_match_price_id,
		 p.relevancy as price_relevancy,
		 p.price,
		 trim(' ' from to_char(p.price,:number_format)) as best_match_price,
		 p.min_price as best_match_min_price,
		 trim(' ' from to_char(p.min_price,:number_format)) as min_price_formatted,
		 p.company_id as price_company_id,
		 p.uom_id as uom_id,
		 p.task_type_id as task_type_id,
		 p.target_language_id as target_language_id,
		 p.source_language_id as source_language_id,
		 p.subject_area_id as subject_area_id,
		 p.file_type_id as file_type_id,
		 p.valid_from,
		 p.valid_through,
		 p.price_note,
		 c.company_path as price_company_name,
			 im_category_from_id(p.uom_id) as price_uom,
			 im_category_from_id(p.task_type_id) as price_task_type,
			 im_category_from_id(p.target_language_id) as price_target_language,
			 im_category_from_id(p.source_language_id) as price_source_language,
			 im_category_from_id(p.subject_area_id) as price_subject_area,
			 im_category_from_id(p.file_type_id) as price_file_type
	 from
		 (

			 (select im_trans_prices_calc_relevancy (
				    p.company_id, :provider_id,
				    p.task_type_id, :task_type_id,
				    p.subject_area_id, :subject_area_id,
				    p.target_language_id, :target_language_id,
				    p.source_language_id, :source_language_id
			         ) as relevancy,
				 p.price_id,
				 p.price,
				 p.min_price,
				 p.company_id,
				 p.uom_id,
				 p.task_type_id,
				 p.target_language_id,
				 p.source_language_id,
				 p.subject_area_id,
				 p.file_type_id,
				 p.valid_from,
				 p.valid_through,
				 p.note as price_note
			 from im_trans_prices p
			 where
				 uom_id=:task_uom_id
				 and currency=:currency
			 )
		 ) p,
		 im_companies c
	 where
		 p.company_id=c.company_id
		 and relevancy >= 0
                 and p.company_id != [im_company_internal]
	 order by
		 p.relevancy desc,
		 p.company_id,
		 p.uom_id
	 limit 1
 "
 if {[db_0or1row reference_prices $references_prices_sql]} {
	ns_log Debug "PRICE:: $price $uom_id $task_type_id --- $best_match_min_price ... $price_company_id "
	 regsub -all {,} $best_match_price {.} best_match_price
	 regsub -all {,} $task_sum {.} task_sum

	 if {!$ignore_min_price_p} {
	     set best_match_min_price [db_string min_price "select min_price from im_trans_prices where price_id = :best_match_price_id" -default 0]	     
		 if {[expr $best_match_price * $task_sum] < $best_match_min_price} {
			 set task_sum 1
			 set task_uom_id [im_uom_unit]
			 set task_uom [im_category_from_id $task_uom_id]
			 set best_match_price $best_match_min_price
		 }
	 }
 } else {
	 set best_match_price 0
	 set best_match_min_price 0
 }

 return [list $task_sum $task_uom_id $best_match_price]

 }

# ------------------------------------------------------
# Price List
# ------------------------------------------------------

ad_proc im_trans_price_component { user_id company_id return_url} {
    Returns a formatted HTML table representing the
    prices for the current company
} {
    if {![im_permission $user_id view_costs]} { return "" }
    
    set enable_file_type_p [parameter::get_from_package_key -package_key intranet-trans-invoices -parameter "EnableFileTypeInTranslationPriceList" -default 0]
    set enable_complexity_type_p [parameter::get_from_package_key -package_key intranet-trans-invoices -parameter "EnableComplexityTypeInTranslationPriceList" -default 0]

    set bgcolor(0) " class=roweven "
    set bgcolor(1) " class=rowodd "
    set price_format "000.000"
    set min_price_format "000.00"
    set price_url_base "/intranet-trans-invoices/price-lists/new"

    set colspan 9
    if {$enable_file_type_p} { incr colspan}

    set file_type_html "<td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.File_Type "File Type"]</td>"
    if {!$enable_file_type_p} { set file_type_html "" }

    if {$enable_complexity_type_p} { incr colspan}
    set complexity_type_html "<td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.Complexity_Type "Complexity Type"]</td>"
    if {!$enable_complexity_type_p} { set complexity_type_html ""}
	
    set price_list_html "
    	<form action=/intranet-trans-invoices/price-lists/price-action method=POST>
	[export_form_vars company_id return_url]
	<table border=0>
	<tr><td colspan=$colspan class=rowtitle align=center>[_ intranet-trans-invoices.Price_List]</td></tr>
	<tr class=rowtitle>
		  <td class=rowtitle>[_ intranet-trans-invoices.UoM]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Task_Type]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Source]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Target]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Subject]</td>
		  $file_type_html
		  $complexity_type_html
		  <td class=rowtitle>[_ intranet-trans-invoices.Rate]</td>
		  <td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.Minimum_Rate "Min Rate"]</td>
		  <td class=rowtitle>[_ intranet-core.Note]</td>
		  <td class=rowtitle>[im_gif -translate_p 1 del "Delete"]</td>
	</tr>
    "

    set price_rows_html ""
    set ctr 1
    set old_currency ""
    db_foreach prices {

select
	p.*,
	c.company_path as company_short_name,
	im_category_from_id(uom_id) as uom,
	im_category_from_id(task_type_id) as task_type,
	im_category_from_id(target_language_id) as target_language,
	im_category_from_id(source_language_id) as source_language,
	im_category_from_id(subject_area_id) as subject_area,
	im_category_from_id(file_type_id) as file_type,
	im_category_from_id(complexity_type_id) as complexity_type,
	to_char(min_price, :min_price_format) as min_price_formatted
from
	im_trans_prices p
      LEFT JOIN
	im_companies c USING (company_id)
where
	p.company_id=:company_id
order by
	im_category_from_id(task_type_id) desc,
	im_category_from_id(subject_area_id) desc,
	im_category_from_id(source_language_id) desc,
	im_category_from_id(target_language_id) desc,
	currency,
	uom_id
} {

        # There can be errors when formatting an empty string...
        set price_formatted ""
        catch { set price_formatted "[format "%0.3f" $price] $currency" } errmsg
	if {"" != $old_currency && ![string equal $old_currency $currency]} {
	    append price_rows_html "<tr><td colspan=$colspan>&nbsp;</td></tr>\n"
	}

        set price_url [export_vars -base $price_url_base { company_id price_id return_url }]

	set file_type_html "<td>$file_type</td>"
	if {!$enable_file_type_p} { set file_type_html "" }
	set complexity_type_html "<td>$complexity_type</td>"
	if {!$enable_complexity_type_p} { set file_complexity_html "" }

	append price_rows_html "
        <tr $bgcolor([expr $ctr % 2]) nobreak>
	  <td>$uom</td>
	  <td>$task_type</td>
	  <td>$source_language</td>
          <td>$target_language</td>
	  <td>$subject_area</td>
	  $file_type_html
	  $complexity_type_html
          <td><a href=\"$price_url\">$price_formatted</a></td>
          <td>$min_price_formatted</td>
          <td>[string_truncate -len 15 $note]</td>
          <td><input type=checkbox name=price_id.$price_id></td>
	</tr>"
	incr ctr
	set old_currency $currency
    }

    if {$price_rows_html != ""} {
	append price_list_html $price_rows_html
    } else {
	append price_list_html "<tr><td colspan=$colspan align=center><i>[_ intranet-trans-invoices.No_prices_found]</i></td></tr>\n"
    }

    set sample_pracelist_link "<a href=/intranet-trans-invoices/price-lists/pricelist_sample.csv>[_ intranet-trans-invoices.lt_sample_pricelist_CSV_]</A>"

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<tr>
	  <td colspan=$colspan align=right>
	    <input type=submit name=add_new value=\"[_ intranet-trans-invoices.Add_New]\">
	    <input type=submit name=del value=\"[_ intranet-trans-invoices.Del]\">
	  </td>
	</tr>
        "
    }

    append price_list_html "
       </table>
       </form>
    "

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<ul>
	  <li>
	    <a href=/intranet-trans-invoices/price-lists/upload-prices?[export_url_vars company_id return_url]>
	      [_ intranet-trans-invoices.Upload_prices]</A>
	    [_ intranet-trans-invoices.lt_for_this_company_via_]
	  <li>
	    [_ intranet-trans-invoices.lt_Check_this_sample_pra]
	    [_ intranet-trans-invoices.lt_It_contains_some_comm]
	  <li>
	    <a href=\"[export_vars -base "/intranet-reporting/view" {{report_code translation_price_list_export} {format csv} company_id}]\">
	    [lang::message::lookup "" intranet-trans-invoices.Export_as_csv "Export price list as CSV"]
	    </a>
	</ul>
        "
    }

    return $price_list_html
}


ad_proc im_trans_prices_populate_providers {
	{-company_id ""}
} {
	Populate Trans prices from previous invoices or bills
} {
	# Get the distinct list of prices

	# task_type_id - im_trans_tasks
	# Target Language ID - im_trans_tasks
	# source_langauge_id
	# subject_area_id
	# price
	# currency


	# Check for each of the assignments
	foreach type [list trans edit proof] {

		# We try to find the correct trans type id. If we have prices maintained though for Trans as well as Trans / Edit, we will most likely not get the proper result, especially not if we have two different Project Types which have
		# Trans on it's own but are referrenced for the same company.
		set task_type_id [db_string task "select category_id from im_categories where aux_string1 = :type and category_type = 'Intranet Project Type' limit 1" -default ""]



		db_foreach tasks "select distinct c.provider_id, tt.target_language_id, tt.source_language_id, p.subject_area_id, tt.task_units, ii.item_uom_id, ii.item_units, ii.price_per_unit, ii.currency from im_trans_tasks tt, im_invoice_items ii,im_costs c, im_projects p,im_companies co where p.project_id = tt.project_id and ii.task_id = tt.task_id and c.cost_id = ii.invoice_id and c.cost_type_id = 3704 and ii.price_per_unit not in (select price from im_trans_prices where uom_id = ii.item_uom_id and company_id = c.provider_id and task_type_id = tt.task_type_id and target_language_id = tt.target_language_id and source_language_id = tt.source_language_id and subject_area_id = p.subject_area_id and currency = ii.currency) and tt.${type}_id = co.primary_contact_id and co.company_id = c.provider_id and item_uom_id in (320,324)
			order by provider_id" {
			if {$price_per_unit eq ""} {continue}
			if {$item_units == "1.0" && $task_units == "1.0"} {continue}
			if {$item_units == "1.0" && $task_units > 1} {
				set price_per_unit [expr $price_per_unit / $task_units]
			}

			set existing_price_per_unit [db_string test "select max(price) from im_trans_prices where uom_id = :item_uom_id and company_id = :provider_id and task_type_id = :task_type_id and target_language_id = :target_language_id and source_language_id = :source_language_id and subject_area_id = :subject_area_id and currency = :currency" -default ""]
			if {$existing_price_per_unit >= $price_per_unit} {
				ns_log Debug "Skipping $provider_id :: $price_per_unit"
				continue
			}
			if {$price_per_unit > 0.3 && $item_uom_id == 324} {
				ns_log Debug "Too high price $price_per_unit :: $provider_id"
				continue
			}
			db_dml delete_prices "delete from im_trans_prices where uom_id = :item_uom_id and company_id = :provider_id and task_type_id = :task_type_id and target_language_id = :target_language_id and source_language_id = :source_language_id and subject_area_id = :subject_area_id and currency = :currency"
			set price_per_unit [format "%.3f" $price_per_unit]
				db_dml price_insert "
				insert into im_trans_prices (
					price_id,
					uom_id,
					company_id,
					task_type_id,
					target_language_id,
					source_language_id,
					subject_area_id,
					currency,
					price
				) values (
					nextval('im_trans_prices_seq'),
					:item_uom_id,
					:provider_id,
					:task_type_id,
					:target_language_id,
					:source_language_id,
					:subject_area_id,
					:currency,
					:price_per_unit
				)"
			ns_log Debug "$provider_id :: $task_type_id :: $target_language_id :: $source_language_id :: $subject_area_id :: $currency$price_per_unit"
		}
	}

	# Transfer the prices from timesheet prices
	# http://kolibri.sussdorff.org/intranet/companies/view?company_id=273588
	db_foreach timesheet_prices {
		select uom_id,company_id,task_type_id,currency,price from im_timesheet_prices
	} {
		switch $task_type_id {
			88 - 10000011 - 10000014 {
				set task_type_id 88
			}
			89 - 93 {
				set task_type_id 93
			}
			default {
				set task_type_id ""
			}
		}

		if {$task_type_id eq ""} {continue}
		set existing_price_per_unit [db_string test "select max(price) from im_trans_prices where uom_id = :uom_id and company_id = :company_id and task_type_id = :task_type_id and target_language_id is null and source_language_id is null and subject_area_id is null and currency = :currency" -default ""]

		if {$existing_price_per_unit >= $price} {
			ns_log Debug "Skipping $provider_id :: $price_per_unit"
			continue
		}
		if {$price > 0.3 && $uom_id == 324} {
			ns_log Debug "Too high price $price :: $provider_id"
			continue
		}

		db_dml price_insert "
		insert into im_trans_prices (
			price_id,
			uom_id,
			company_id,
			task_type_id,
			target_language_id,
			source_language_id,
			subject_area_id,
			currency,
			price
		) values (
			nextval('im_trans_prices_seq'),
			:uom_id,
			:company_id,
			:task_type_id,
			NULL,
			NULL,
			NULL,
			:currency,
			:price
		)"
		ns_log Debug "inserting trans price $price :: $company_id :: $uom_id"
	}

	# update the price types
	db_dml update "update im_trans_prices set task_type_id = 93 where task_type_id in (87,89,94,2500)"
}

ad_proc -public im_trans_price_history_new {
	-project_id
	{-uom_id ""}
	-company_id
	{-task_type_id ""}
	{-target_language_id ""}
	{-source_language_id ""}
	{-subject_area_id ""}
	{-file_type_id ""}
	{-complexity_type_id ""}
	{-valid_from ""}
	{-valid_through ""}
	{-currency ""}
	-price
	{-min_price ""}
	{-note ""}
} {
	Enter a new entry into the history table for prices
} {
	db_dml insert_price_sql "INSERT INTO im_trans_price_history (
		project_id, uom_id, company_id, task_type_id,
		target_language_id, source_language_id, subject_area_id,
		file_type_id, complexity_type_id,
		   valid_from, valid_through, currency, price, min_price, note
		) VALUES (
		:project_id, :uom_id, :company_id, :task_type_id,
		:target_language_id, :source_language_id, :subject_area_id,
		:file_type_id, :complexity_type_id,
		:valid_from, :valid_through, :currency, :price, :min_price, :note
	)"
	return 1
}
