ad_proc -public -callback im_invoice_after_create -impl im_trans_invoices_price_history_recording {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Record a price which is not in the price list of the provider
} {
	# Check if this is a provider_bill
	if {[db_string provider_bill_p "select 1 from im_costs where cost_id = :object_id and cost_type_id = [im_cost_type_bill]" -default 0]} {
		db_1row  cost_info "select project_id, provider_id from im_costs where cost_id = :object_id"

		# Find items where the source_language_id is not null.
		# Those most likely are translation items
		db_foreach item_ids "select item_id, source_language_id, target_language_id, task_type_id, subject_area_id, currency, price_per_unit as rate, item_uom_id as rate_uom_id from im_invoice_items ii, im_materials m where invoice_id = :object_id and m.material_id = ii.item_material_id and m.source_language_id is not null" {
	
			# Check if we have the rate already in the price list for the freelancer
			set list_prices [db_list rate_p "select price from im_trans_prices
				where company_id = :provider_id
				and task_type_id = :task_type_id
				and uom_id = :rate_uom_id
				and currency = :currency"]
			
			if {[lsearch $list_prices $rate]<0} {
				# The rate seems not to be standard
				
				# Check if we have the rate already in the history for the freelancer
				set rate_p [db_string rate_p "select 1 from im_trans_price_history
					where uom_id = :rate_uom_id
					and   company_id = :provider_id
					and   task_type_id = :task_type_id
					and   target_language_id = :target_language_id
					and   source_language_id = :source_language_id
					and   subject_area_id = :subject_area_id
					and   currency = :currency limit 1" -default 0]
			
				if {!$rate_p} {
					db_dml price_insert "
					insert into im_trans_price_history (project_id,
						uom_id,
						company_id,
						task_type_id,
						target_language_id,
						source_language_id,
						subject_area_id,
						currency,
						price
					) values ($project_id,
						:rate_uom_id,
						:provider_id,
						:task_type_id,
						:target_language_id,
						:source_language_id,
						:subject_area_id,
						:currency,
						:rate
					)"
				}
			}
		}
	}
}
