SELECT acs_log__debug('/packages/intranet-trans-invoices/sql/postgresql/upgrade/upgrade-5.0.2.4.0-5.0.2.4.1.sql','');


-- Categories
SELECT im_category_new ('4290', 'Default', 'Intranet Translation Complexity');
SELECT im_category_new ('4291', 'Premium', 'Intranet Translation Complexity');

-- Adding new column to im_trans_prices
create or replace function inline_0()
returns integer as $$
DECLARE
	v_count		integer;
BEGIN
     SELECT count(column_name) into v_count
     FROM information_schema.columns
     WHERE table_name='im_trans_prices' and column_name='complexity_type_id';

     IF v_count > 0 THEN return 1; END IF;

     ALTER TABLE im_trans_prices ADD COLUMN complexity_type_id INTEGER NOT NULL DEFAULT 4290;
     return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0()
returns integer as $$
DECLARE
	v_count		integer;
BEGIN
     SELECT count(column_name) into v_count
     FROM information_schema.columns
     WHERE table_name='im_trans_price_history' and column_name='complexity_type_id';

     IF v_count > 0 THEN return 1; END IF;

     ALTER TABLE im_trans_price_history ADD COLUMN complexity_type_id INTEGER NOT NULL DEFAULT 4290;
     return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

update im_reports set report_sql = '
	select
		im_category_from_id(tp.uom_id) as uom,
		c.company_path,
		im_category_from_id(tp.task_type_id) as task_type,
		im_category_from_id(tp.target_language_id) as target_language,
		im_category_from_id(tp.source_language_id) as source_language,
		im_category_from_id(tp.subject_area_id) as subject_area,
		im_category_from_id(tp.file_type_id) as file_type,
		im_category_from_id(tp.complexity_type_id) as complexity_type,
		tp.valid_from,
		tp.min_price,
		tp.valid_through,
		replace(to_char(tp.price, ''99999.99''), ''.'', '','') as price,
		tp.currency
	from	im_trans_prices tp,
		im_companies c
	where	tp.company_id = c.company_id and
		tp.company_id = %company_id%
	order by
		im_category_from_id(tp.uom_id),
		c.company_path,
		im_category_from_id(tp.task_type_id),
		im_category_from_id(tp.target_language_id),
		im_category_from_id(tp.source_language_id),
		im_category_from_id(tp.subject_area_id),
	im_category_from_id(tp.file_type_id)' where report_code = 'translation_price_list_export';


	-- New procedure with added filetype
create or replace function im_trans_prices_calc_relevancy ( 
	integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer, integer
) returns numeric as '
DECLARE
	v_price_company_id		alias for $1;		
	v_item_company_id		alias for $2;
	v_price_task_type_id		alias for $3;	
	v_item_task_type_id		alias for $4;
	v_price_subject_area_id		alias for $5;	
	v_item_subject_area_id		alias for $6;
	v_price_target_language_id	alias for $7;	
	v_item_target_language_id	alias for $8;
	v_price_source_language_id	alias for $9;	
	v_item_source_language_id	alias for $10;
	v_price_file_type_id		alias for $11;
	v_item_file_type_id		alias for $12;
	v_price_complexity_type_id		alias for $13;
	v_item_complexity_type_id		alias for $14;

	match_value			numeric;
	v_internal_company_id		integer;
	v_price_target_language		varchar(100);
	v_item_target_language		varchar(100);
	v_price_source_language		varchar(100);
	v_item_source_language		varchar(100);
BEGIN
	match_value := 0;

	select company_id
	into v_internal_company_id
	from im_companies
	where company_path=''internal'';

	-- Hard matches for task type
	if v_price_task_type_id = v_item_task_type_id then
		match_value := match_value + 8;
	end if;
	if not(v_price_task_type_id is null) and v_price_task_type_id != v_item_task_type_id then
		match_value := match_value - 8;
	end if;

	-- Default matching for source language:
	-- "de" <-> "de_DE" = + 1
	-- "de_DE" <-> "de_DE" = +3
	-- "es" <-> "de_DE" = -10
	if (v_price_source_language_id is not null) and  (v_item_source_language_id is not null) then
		-- only add or subtract match_values if both are defined...
		select	category
		into	v_price_source_language
		from	im_categories
		where	category_id = v_price_source_language_id;
	
		select	category
		into	v_item_source_language
		from	im_categories
		where	category_id = v_item_source_language_id;

		if lower(substr(v_price_source_language,1,2)) = lower(substr(v_item_source_language,1,2)) then
			-- the main part of the language have matched
			match_value := match_value + 2;
			if v_price_source_language_id = v_item_source_language_id then
				-- the main part have matched and the country variants are the same
				match_value := match_value + 1;
			end if;
		else
			match_value := match_value - 20;
		end if;
	end if;


	-- Default matching for target language:
	if (v_price_target_language_id is not null) and  (v_item_target_language_id is not null) then
		-- only add or subtract match_values if both are defined...
		select	category
		into	v_price_target_language
		from	im_categories
		where	category_id = v_price_target_language_id;
	
		select	category
		into	v_item_target_language
		from	im_categories
		where	category_id = v_item_target_language_id;

		if lower(substr(v_price_target_language,1,2)) = lower(substr(v_item_target_language,1,2)) then
			-- the main part of the language have matched
			match_value := match_value + 1;		
			if v_price_target_language_id = v_item_target_language_id then
				-- the main part have matched and the country variants are the same
				match_value := match_value + 1;
			end if;
		else
			match_value := match_value - 20;
		end if;
	end if;

	-- Subject Area
	if v_price_subject_area_id = v_item_subject_area_id then
		match_value := match_value + 1;
	end if;
	if not(v_price_subject_area_id is null) and v_price_subject_area_id != v_item_subject_area_id then
		match_value := match_value - 20;
	end if;

	-- Company logic - "Internal" doesnt give a penalty 
	-- but doesnt count as high as an exact match
	--
	if v_price_company_id = v_item_company_id then
		match_value := (match_value + 6)*2;
	end if;
	if v_price_company_id = v_internal_company_id then
		match_value := match_value + 1;
	end if;
	if v_price_company_id != v_internal_company_id and v_price_company_id != v_item_company_id then
		match_value := match_value -100;
	end if;


	-- File Type
	if v_price_file_type_id = v_item_file_type_id then
		match_value := match_value + 1;
	end if;
	if not(v_price_file_type_id is null) and v_price_file_type_id != v_item_file_type_id then
		match_value := match_value - 10;
	end if;

	-- Complexity Type
	if v_price_complexity_type_id = v_item_complexity_type_id then
		match_value := match_value + 1;
	end if;
	if not(v_price_complexity_type_id is null) and v_price_complexity_type_id != v_item_complexity_type_id then
		match_value := match_value - 20;
	end if;

	return match_value;
end;' language 'plpgsql';


