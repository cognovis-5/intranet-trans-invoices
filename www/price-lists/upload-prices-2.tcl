# /packages/intranet-trans-invoices/www/upload-prices-2.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    /intranet/companies/upload-prices-2.tcl
    Read a .csv-file with header titles exactly matching
    the data model and insert the data into im_trans_prices
} {
    return_url
    company_id:integer
    upload_file
}

set current_user_id [ad_maybe_redirect_for_registration]
if {![im_permission $current_user_id add_costs]} {
    ad_return_complaint 1 "[_ intranet-trans-invoices.lt_You_have_insufficient_1]"
    return
}


set page_title "Upload New File/URL"
set context_bar [im_context_bar [list "/intranet/cusomers/" "Clients"] "Upload CSV"]

set tmp_filename [ns_queryget upload_file.tmpfile]
set errors [cog::price::import_price_list -csv_path $tmp_filename -company_id $company_id -user_id $current_user_id]
if {$errors eq ""} {
    ad_returnredirect $return_url
} else {
    set error_body "[join $errors "<br />"]"
    ad_return_template
}